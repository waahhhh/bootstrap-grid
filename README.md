Bootstrap V4.0.0-beta grid only  
This Bootstrap Grid contains only grid col-sm-6.

Regex:
```(\.col\-sm\-6|\.row|\.container\-fluid)```

Source: https://github.com/twbs/bootstrap/blob/v4-dev/dist/css/bootstrap-grid.css

minified by https://csscompressor.com

## Overview

|grid system|size|min size|
|---|---|---|
|original|30.5 KB (1567 lines)|23.6 KB (7 lines)|
|current|999 Byte (50 lines)|784 Byte (6 lines)|

date: 2017-10-30 14:11 UTC